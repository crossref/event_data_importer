(ns event-data-importer.core
  (:require [event-data-importer.scholix :refer [scholix->event]]
            [event-data-importer.relations :as relations]
            [event-data-importer.lookup :as lookup]
            [event-data-common.event-bus :as bus]
            [clojure.tools.logging :as log]
            [clojure.data.json :as json]
            [clojure.tools.cli :refer [parse-opts]]
            [config.core :refer [env]]
            [clojure.java.io :refer [writer reader as-file file]]))

(defn scholix-file
  [input-filename
   output-filename
   {:keys [reverse? lookup?]}]

  (let [input-file (as-file input-filename)
        output-file (as-file output-filename)]

    (when-not (.exists input-file)
      (log/error "Input file does not exist! Exiting.")
      (System/exit 1))

    (let [num-lines (with-open [f (reader input-file)]
                      (-> f line-seq count))
          done (atom 0)]

     (with-open [out (writer output-file)
                 in (reader input-file)]
       (let [objects (->> in line-seq (map #(json/read-str % :key-fn keyword)))
             events (map scholix->event objects)
             
             ; Apply options.
             patched (cond->> events
                       reverse? (map relations/reverse-relation-in-event)
                       lookup? (map lookup/lookup-content-types-in-event))]

         (doseq [event patched]
           (.write out (json/write-str event))
           (.write out "\n")

           ; Low volume, can be useful to tail the output.
           (.flush out)
           (swap! done inc)
           (when (zero? (rem @done 100))
             (println "Done" @done "/" num-lines "lines, "
                      (int (* (/ @done num-lines) 100)) "%"))))))))

(defn scholix-dir
  [input-dirname output-dirname]
  (let [files (-> input-dirname file .listFiles)
        output-dir (file output-dirname)]

    (doseq [f files]
      (log/info "Process" f)
      ; Same filename in corresponding output dir.
      (scholix-file f (file output-dir (.getName f))))))

(defn upload
  [input-files]
  (log/info "Upload" (count input-files) "files")
  (doseq [input-file input-files]
    (log/info "Uploading" input-file)
      (with-open [in (reader input-file)]
        (let [events (->> in line-seq (map #(json/read-str % :key-fn keyword)))]
          (doseq [event events]
            (bus/post-event event))))))

(def cli-options
  [["-r" "--reverse" "Reverse relationship direction."
    :id :reverse?
    :default false]
   ["-l" "--lookup" "Ignore supplied content type and look up."
    :id :lookup?
    :default false]])

(defn -main [& args]
  (let [{:keys [arguments options exit-message ok?]} (parse-opts args cli-options)
        [action input output] arguments]

    (if exit-message
      (do
       (prn exit-message)
       (System/exit (if ok? 0 1)))
      (case action
        "scholix-file" (scholix-file input output options)
        "scholix-dir" (scholix-dir input output)
        "upload" (upload input)
        (prn "Didn't recognise action.")))))

