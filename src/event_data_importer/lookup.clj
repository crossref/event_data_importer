(ns event-data-importer.lookup
  "Look up content types."
  (:require [crossref.util.doi :as cr-doi]
            [crossref.util.string :as cr-str]
            [clj-http.client :as client]
            [clojure.data.json :as json]
            [clojure.tools.logging :as log]
            [robert.bruce :refer [try-try-again]]
            [config.core :refer [env]]
            [clojure.string :as string])

  (:import [java.net URLEncoder]))

(def work-type-name :work)

(defn doi->id
  [doi]
  (some-> doi cr-doi/normalise-doi cr-str/md5))

(defn get-ra-api
  "Get the Registration Agency from the DOI RA API. 
   Return :crossref :datacite or nil."
  [non-url-normalized-doi]
  (when non-url-normalized-doi
    (try
      (-> non-url-normalized-doi
          (URLEncoder/encode "UTF-8")
          (#(str "https://doi.org/doiRA/" %))
          client/get
          :body
          (json/read-str :key-fn keyword)
          first
          :RA
          (or "")
          string/lower-case
          {"datacite" :datacite
           "crossref" :crossref})

      ; Not found, or invalid data, return nil.
      (catch Exception ex (do (log/error ex) nil)))))

(defn get-work-api
  "Get the work metadata from the Crossref or DataCite API."
  [non-url-normalized-doi]
  
  ; We might get nils.
  (when non-url-normalized-doi
    (try
      (let [ra (get-ra-api non-url-normalized-doi)
            safe-doi (URLEncoder/encode non-url-normalized-doi "UTF-8")
            url (condp = ra
                  :crossref (str "https://api.crossref.org/v1/works/" safe-doi "?mailto=eventdata@crossref.org")
                  :datacite (str "https://api.datacite.org/works/" safe-doi "?include=resource-type")
                  nil)

            response (try-try-again
                        {:sleep 10000 :tries 2}
                        ; Only retry on genuine exceptions. 404 etc won't be fixed by retrying.
                        #(when url (client/get url {:throw-exceptions false})))
            body (when (= 200 (:status response)) (-> response :body (json/read-str :key-fn keyword)))
            work-type (condp = ra
                        :crossref (-> body :message :type)
                        :datacite (-> body :data :attributes :resource-type-id)
                        nil)]
      ; If we couldn't discover the RA, then this isn't a real DOI. 
      ; Return nil so this doens't get cached (could produce a false-negative in future).

      (when (and ra work-type)
        {:content-type work-type :doi non-url-normalized-doi}))
      (catch Exception ex
        (do
          (log/error "Failed to retrieve metadata for DOI" non-url-normalized-doi "error:" (str ex))
          nil)))))

(defn get-for-doi
  [doi]
  (let [normalized-doi (when (cr-doi/well-formed doi)
                             (cr-doi/non-url-doi doi))]        
    (get-work-api normalized-doi)))

(defn get-type-for-doi
  [doi]
  (-> doi get-for-doi :content-type))

(defn lookup-content-types-in-event
  [event]
  (-> event
      (assoc-in [:subj :work_type_id] (get-type-for-doi (:subj_id event)))
      (assoc-in [:obj :work_type_id] (get-type-for-doi (:obj_id event)))))

