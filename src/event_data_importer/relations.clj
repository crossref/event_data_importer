(ns event-data-importer.relations
 "Functions for working with relations.
  Limited to cases where it's needed. Expand as necessary.")

(def types
 [{:forward "references"
   :backward "is_referenced_by"
   :scholix-foward "References"
   :scholix-backward "IsReferencedBy"}])

; Sanity check that foward and backward relations are distinct.
(assert (->> types (map :forward) (apply distinct?)))
(assert (->> types (map :backward) (apply distinct?)))

(def inverse-relations
 "Map of relations to their inverse."
 (into {} 
  (concat
    (map (juxt :forward :backward) types)
    (map (juxt :backward :forward) types))))

(def scholix->type*
 "Map of relations to their scholix relations."
 (into {} 
  (concat
    (map (juxt :scholix-foward :forward) types)
    (map (juxt :scholix-backward :backward) types))))

(defn scholix->type
  [type-id]
  {:post [(some? %)]}
  (scholix->type* type-id))

(defn reverse-relation-in-event
 [event]
 {:post [(:relation_type_id %)]}
 (-> event
     (assoc :relation_type_id
      (-> event :relation_type_id inverse-relations))))

