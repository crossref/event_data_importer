(ns event-data-importer.relations-test
  (:require [clojure.test :refer :all]
            [event-data-importer.relations :as relations]))

(deftest reverse-relation-in-event
  (testing "Known relations can be reversed, other fields of event unchanged."
    (is (= (relations/reverse-relation-in-event
            {:a :b :relation_type_id "is_referenced_by"})
           {:a :b :relation_type_id "references"}))

    (is (= (relations/reverse-relation-in-event
            {:a :b :relation_type_id "references"})
           {:a :b :relation_type_id "is_referenced_by"})))

  (testing "Unknown relations trigger assertion and exit."
    (is (thrown? java.lang.AssertionError
        (relations/reverse-relation-in-event
          {:a :b :relation_type_id "no_such_relation"})))))

(deftest type->scholix
  (testing "Scholix relation can be converted to Event Data relation type."
    (is (= (relations/scholix->type "References")
           "references")))

  (testing "Unknown relations trigger assertion and exit."
    (is (thrown? java.lang.AssertionError
          (relations/scholix->type "NoSuchThing")))))

